using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Models;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Gateways
{
    public interface IDistributedCache
    {
        Task<bool> AddPreferencesToCache(Guid redisKey, IEnumerable<PreferenceResponse> preferenceResponses);
        Task<IEnumerable<PreferenceResponse>> GetPreferencesFromCache(Guid redisKey);
    }
}