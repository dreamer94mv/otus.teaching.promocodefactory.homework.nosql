﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Gateways;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения клиентов
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController
        : ControllerBase
    {
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IDistributedCache _distributedCache;

        public PreferencesController(IRepository<Preference> preferencesRepository,
            IDistributedCache distributedCache)
        {
            _preferencesRepository = preferencesRepository;
            _distributedCache = distributedCache;
        }
        
        /// <summary>
        /// Получить список предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PreferenceResponse>>> GetPreferencesAsync(Guid redisKey)
        {
            var cache = await _distributedCache.GetPreferencesFromCache(redisKey);

            if (cache != null)
            {
                return Ok(cache);
            }
            else
            {
                var preferences = await _preferencesRepository.GetAllAsync();
                var response = preferences.Select(x => new Core.Models.PreferenceResponse()
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList();

                await _distributedCache.AddPreferencesToCache(redisKey, response);

                return Ok(response);
            }            
        }
    }
}